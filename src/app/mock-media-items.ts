import {MediaItem} from './models/media-item.model';

export const mediaItems: MediaItem[] = [
  {
    imagePath: 'assets/images/audio-image.png',
    name: 'A Conference of Elon Musk',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry' +
      '. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
    dateUploaded: new Date('12/03/2019'),
    type: 'audio'
  },
  {
    imagePath: 'assets/images/image-image.jpg',
    name: 'B Image of Angular logo',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry' +
      '. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
    dateUploaded: new Date('11/03/2019'),
    type: 'image'
  },
  {
    imagePath: 'assets/images/audio-image.png',
    name: 'Another audio file',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry' +
      '. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
    dateUploaded: new Date('10/03/2019'),
    type: 'audio'
  }
  ,
  {
    imagePath: 'assets/images/document-image.png',
    name: 'Name 1 of an document',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry' +
      '. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
    dateUploaded: new Date('10/03/2017'),
    type: 'document'
  },
  {
    imagePath: 'assets/images/document-image.png',
    name: 'Some other file',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry' +
      '. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
    dateUploaded: new Date('10/04/2018'),
    type: 'document'
  },
  {
    imagePath: 'assets/images/document-image.png',
    name: 'You will find pdf in here',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry' +
      '. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
    dateUploaded: new Date('10/07/2019'),
    type: 'document'
  },
  {
    imagePath: 'assets/images/video-image.png',
    name: 'A video in here',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry' +
      '. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
    dateUploaded: new Date('01/05/2019'),
    type: 'video'
  },
  {
    imagePath: 'assets/images/video-image.png',
    name: 'Flutter presentation',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry' +
      '. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
    dateUploaded: new Date('01/01/2019'),
    type: 'video'
  },
  {
    imagePath: 'assets/images/image-image.jpg',
    name: 'Javascript of the future',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry' +
      '. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
    dateUploaded: new Date('11/03/2010'),
    type: 'image'
  },
  {
    imagePath: 'assets/images/image-image.jpg',
    name: 'Sunset',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry' +
      '. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
    dateUploaded: new Date('11/03/2005'),
    type: 'image'
  },
];
