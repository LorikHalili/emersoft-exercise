import {Component, OnInit} from '@angular/core';
import {MediaItemService} from '../../services/media-item.service';
import {MediaItem} from '../../models/media-item.model';
import {merge, Observable} from 'rxjs';
import {FormBuilder, FormGroup} from '@angular/forms';
import {debounceTime, distinctUntilChanged, map, switchMap} from 'rxjs/operators';
interface Filters {
  search: string;
  filter: string;
  sort: string;
  sortDirection: boolean;
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  filters: any[] = [
    {value: '', viewValue: 'All Media'},
    {value: 'video', viewValue: 'Videos'},
    {value: 'audio', viewValue: 'Audio'},
    {value: 'image', viewValue: 'Images'},
    {value: 'document', viewValue: 'Documents'}
  ];
  sorts: any[] = [
    {value: 'dateUploaded', viewValue: 'Date Uploaded'},
    {value: 'name', viewValue: 'Alphabetical'},
  ];
  mediaItems: MediaItem[] = [];
  loading: boolean;
  filterForm: FormGroup;

  searchValue$: Observable<string>;
  filterValue$: Observable<string>;
  sortValue$: Observable<string>;
  sortDirectionValue$: Observable<string>;
  constructor(
    private mediaService: MediaItemService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.initFilterForm();
    this.getMediaItems();
    this.searchValue$ = this.filterForm.get('search').valueChanges.pipe(
      debounceTime(200),
      distinctUntilChanged(),
    );
    this.filterValue$ = this.filterForm.get('filter').valueChanges.pipe(
      debounceTime(100),
      distinctUntilChanged(),
    );
    this.sortValue$ = this.filterForm.get('sort').valueChanges.pipe(
      debounceTime(100),
      distinctUntilChanged(),
    );
    this.sortDirectionValue$ = this.filterForm.get('sortDirection').valueChanges.pipe(
      debounceTime(100),
      distinctUntilChanged(),
    );
    merge(this.searchValue$, this.filterValue$, this.sortValue$, this.sortDirectionValue$).subscribe((res) => {
      this.getMediaItems();
    });
  }
  initFilterForm() {
    this.filterForm = this.fb.group({
      search: [''],
      filter: [''],
      sort: [''],
      sortDirection: [''],
    });
  }
  getMediaItems() {
    this.loading = true;
    const filterFormValue = this.filterForm.value as Filters;
    this.mediaService.getMediaItems().pipe(
      map(results => {
        results = JSON.parse(JSON.stringify(results)); // Creating a new instance of results
        if (filterFormValue.filter) {
          results = results.filter(x => {
            return x.type === filterFormValue.filter;
          });
        }
        if (filterFormValue.search) {
            results = results.filter(x => {
              return x.name.toLowerCase().indexOf(filterFormValue.search.toLowerCase()) !== -1;
            }
          );
        }
        if (filterFormValue.sort) {
          results = results.sort((a, b) => {
            const A = a[filterFormValue.sort];
            const B = b[filterFormValue.sort];
            if (A < B) {
              return -1;
            }
            if (A > B) {
              return 1;
            }

            return 0;
          });
          return filterFormValue.sortDirection || filterFormValue.sortDirection == null ? results : results.reverse();
        }
        return results;
      })
    ).subscribe(results => {
      this.loading = false;
      this.mediaItems = results;
    });
  }
  changeSortDirection(asc: boolean) {
    this.filterForm.get('sortDirection').setValue(asc);
  }
  clearFilters() {
    this.filterForm.reset();
  }
}
