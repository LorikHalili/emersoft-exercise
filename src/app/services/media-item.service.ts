import { Injectable } from '@angular/core';
import {MediaItem} from '../models/media-item.model';
import {mediaItems} from '../mock-media-items';
import {Observable, of} from 'rxjs';
import {delay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MediaItemService {

  mediaItems: MediaItem[] = mediaItems;
  constructor() { }

  getMediaItems(): Observable<MediaItem[]> {
    return of(this.mediaItems).pipe(delay(500));
  }
}
