export class MediaItem {
  imagePath: string;
  name: string;
  description: string;
  dateUploaded: Date;
  type: 'document' | 'image' | 'audio' | 'video';
}
